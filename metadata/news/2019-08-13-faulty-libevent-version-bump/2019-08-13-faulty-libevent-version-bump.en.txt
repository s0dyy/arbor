Title: Faulty libevent version bump
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2019-08-13
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-libs/libevent:2.1[=2.1.11]

dev-libs/libevent was bumped to 2.1.11 but the new version contained
an increased soversion, prompting to run cave fix-linkage despite the
package already being slotted.

This has been addressed in [1] by bringing back the old version and
[2] moving 2.1.11-r1 to a new SLOT. But if you already installed
2.1.11 (without -r1) you'll need to run

# cave resolve libevent:2.1 --permit-downgrade libevent:2.1
# cave resolve libevent:7

If you already rebuilt some of its dependents, unfortunately you'll
also need to execute

# cave fix-linkage

again. Alternatively you can run

# cave resolve -D libevent:2.1 nothing

Sorry for the inconvenience.

[1] aef856ff08d274d891ad3026231834cbce83df40
[2] 1cd36a444fd5a238a7b8c53fa297b5051210fe39
