# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ]

SUMMARY="GNU implementation of the M4 macro processor"

REMOTE_IDS="freecode:gnu${PN}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    parts: binaries documentation
    ( linguas: bg cs da de el eo es fi fr ga gl hr id ja ko nl pl pt_BR ro ru sr sv uk vi zh_CN
               zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.2]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    ac_cv_libsigsegv=no # Do not link against libsigsegv
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( BACKLOG )

src_install() {
    default

    expart binaries /usr/$(exhost --target)/bin
    expart documentation /usr/share/{doc,info,man}
}

src_test() {
    esandbox allow_net "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    default

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net "inet:127.0.0.1@80"
}

pkg_postinst() {
    if [[ $(readlink -f "${ROOT##/}"/usr/bin) == ${ROOT##/}/usr/bin ]] ; then
        nonfatal edo ln -sf ../${CHOST}/bin/m4 "${ROOT}"/usr/bin/m4 ||
            eerror "Copying /usr/bin/m4 failed"
    fi
}

