# Copyright 2018 Thomas Berger <loki@lokis-chaos.de>
# Copyright 2008, 2009, 2010, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases pkg_pretend pkg_setup src_prepare src_compile src_test src_install pkg_pretend pkg_postinst

require alternatives systemd-service [ systemd_files=[ postgresql.service ] ]
require dependencies-helpers providers

# normaly i would say has_lib=false, but then we don't get the python suboptions
if ever at_least 15; then
    require python [ multibuild=false with_opt=true blacklist=2 ]
else
    require python [ multibuild=false with_opt=true blacklist=none ]
fi

SLOT=$(ever major)

SUMMARY="PostgreSQL is a powerful, open source relational database system"
DESCRIPTION="
PostgreSQL is a powerful, open source relational database system.
It has more than 15 years of active development and a proven architecture that has earned it
a strong reputation for reliability, data integrity, and correctness.
It runs on all major operating systems, including Linux, UNIX (AIX, BSD, HP-UX, SGI IRIX, Mac OS X, Solaris, Tru64), and Windows.
It is fully ACID compliant, has full support for foreign keys, joins, views, triggers, and stored procedures (in multiple languages).
It includes most SQL92 and SQL99 data types, including INTEGER, NUMERIC, BOOLEAN, CHAR, VARCHAR, DATE, INTERVAL, and TIMESTAMP.
It also supports storage of binary large objects, including pictures, sounds, or video.
It has native programming interfaces for C/C++, Java, .Net, Perl, Python, Ruby, Tcl, ODBC, among others,
and exceptional documentation.
"
HOMEPAGE="https://www.postgresql.org"

REMOTE_IDS="github:postgres/postgres"

UPSTREAM_DOCUMENTATION="
${HOMEPAGE}/docs/manuals/           [[ lang = en description = [ PostgreSQL Manuals ] ]]
${HOMEPAGE}/docs/faq/               [[ lang = en description = [ FAQ ] ]]
${HOMEPAGE}/support/security.html   [[ lang = en description = [ Security Information ] ]]
${HOMEPAGE}/docs/${SLOT}/static/upgrading.html [[ lang = en description = [ Upgrade path options ] ]]
${HOMEPAGE}/docs/${SLOT}/static/server-start.html [[ lang = en description = [ Service setup ] ]]
"
UPSTREAM_RELEASE_NOTES="
${HOMEPAGE}/docs/${SLOT}/static/release-${PV//./-}.html
"
DOWNLOADS="mirror://${PN}/source/v${PV}/${PNV}.tar.bz2"

LICENCES="PostgreSQL"

MYOPTIONS="
    doc
    icu [[ description = [ Use ICU for collation and indexes instead of libc. Changing this may require reindexing tables ] ]]
    ldap
    llvm [[ description = [ JIT support based on LLVM ] ]]
    pam
    perl [[ description = [ Support for both PL/Perl and Pl/PerlU server-side programming languages ] ]]
    python [[ description = [ Support for PL/Python (only the \"untrusted\" version) server-side programming language ] ]]
    ssl
    ssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    systemd
    tcl [[ description = [ Support for both PL/Tcl and PL/TclU server-side programming languages ] ]]
    xml [[ description = [ Add core support for XML data type ] ]]
    (
        postgresql_extensions:
            amcheck [[ description = [ A module that allows you to verify the logical consistency of the structure of indexes ] ]]
            auth_delay [[ description = [ Adds a delay to failed authentication responses to make brute-force attacks on password more difficult ] ]]
            auto_explain [[ description = [ Logs execution plans of slow statements automatically ] ]]
            bloom [[ description = [ An index access method based on Bloom filters ] ]]
            btree_gin [[ description = [ GIN operator classes that implement B-tree equivalent behavior for several data types ] ]]
            btree_gist [[ description = [ GiST index operator classes that imlement B-tree equivalent behavior for several data types ] ]]
            citext [[ description = [ The citext data type, a case-insensitive character string type ] ]]
            cube [[ description = [ The cube data type for representing multidimensional cubes ] ]]
            dblink [[ description = [ Connect to other PostgreSQL databases from within a database session ] ]]
            dict_int [[ description = [ An example dictionnary for indexing integers ] ]]
            dict_xsyn [[ description = [ An example dictionnary for replacing words with their synonyms for full-text search ] ]]
            earthdistance [[ description = [ Two methods for calculating great circle distances on the surface of the Earth ]
                             requires    = [ postgresql_extensions: cube ] ]]
            file_fdw [[ description = [ A foreign-data wrapper for accessing files on the servers's file system ] ]]
            fuzzystrmatch [[ description = [ Distance functions on text (mostly for phonetic comparisons) ] ]]
            hstore [[ description = [ The hstore data type ] ]]
            intagg [[ description = [ An integer aggregator and an enumerator (obsolete) ] ]]
            intarray [[ description = [ Additional functions and indices for int arrays and int sets ] ]]
            isn [[ description = [ Data types for international product numbering standards ] ]]
            lo [[ description = [ The large object support functions and the vacuumlo command line utility ] ]]
            ltree [[ description = [ The ltree data type for hierarquical categorization ] ]]
            oid2name [[ description = [ Utility program that helps administrators to examine the file structure used by PostgreSQL ] ]]
            pageinspect [[ description = [ Low-level functions to inspect pages (for debugging) ] ]]
            passwordcheck [[ description = [  The passwordcheck module checks users' passwords whenever they are set with CREATE ROLE or ALTER ROLE ] ]]
            pg_buffercache [[ description = [ Analyze what's happening in the shared buffer cache ] ]]
            pg_freespacemap [[ description = [ Analyze the free space map ] ]]
            pg_prewarm [[ description = [ Load relation data into buffer cache ] ]]
            pg_trgm [[ description = [ Trigram support for Full-Text Search ] ]]
            pg_visibility [[ description = [ Provides a means for examining the visibility map (VM) and page-level visibility information of a table ] ]]
            pgcrypto [[ description = [ Cryptographic (hashing, {de,en}cryption, (de)compression etc) functions ] ]]
            pgstattuple [[ description = [ Functions for tuple-level statistics ] ]]
            postgres_fdw [[ description = [ Extension with the Foreign Data Wrapper to access other PostgreSQL servers ] ]]
            seg [[ description = [ The seg data type for representing line segments or floating point intervals ] ]]
            sepgsql [[ description = [ Loadable module that supports label-based mandatory access control (MAC) based on SELinux security policy ] ]]
            spi [[ description = [ Package of autoinc, insert_username, moddatetime and refint extensions ] ]]
            sslinfo [[ description = [ Functions for retrieving informations about the client SSL certificate ]
                       requires    = [ ssl ] ]]
            tablefunc [[ description = [ Some set returning functions (normal_rand, crosstab variations and connectby) ] ]]
            tcn [[ description = [ A trigger function to notify listeners of changes ] ]]
            test_decoding [[ description = [ Example of a logical decoding plugin ] ]]
            tsm_system_rows [[ description = [ Row-based table sampling method ] ]]
            tsm_system_time [[ description = [ Time-based table sampling method ] ]]
            unaccent [[ description = [ A text-search dictionnary which removes accents from lexemes ] ]]
            uuid-ossp [[ description = [ Functions for generating UUIDs ] ]]
            xml2 [[ description = [ XPath querying and XSLT functionality (deprecated by upstream; planned for removal) ]
                    requires    = [ xml ] ]]
    )
"

if ! ever at_least 14; then
    MYOPTIONS+="
        (
            postgresql_extensions:
                pg_standby [[ description = [ pg_standby supports creation of a warm standby database server ] ]]
        )
    "
else
    MYOPTIONS+="
        lz4 [[ description = [ Allow configurable LZ4 TOAST compression ] ]]
    "
fi

if ever at_least 15; then
    MYOPTIONS+="
        zstd [[ description = [ Allow configurable Zstandard compression ] ]]
        (
            postgresql_extensions:
                basebackup_to_shell [[ description = [ Adds a custom basebackup target called shell primarily intended as an example ] ]]
                basic_archive [[ description = [ Example of an archive module supposed to serve as a starting point for development ] ]]
                pg_walinspect [[ description = [ Provides SQL functions that allow you to inspect the contents of write-ahead log ] ]]
        )
    "
fi

DEPENDENCIES="
    !dev-db/postgresql:0 [[
        description = [ Slotify PostgreSQL ]
        resolution = uninstall-blocked-after
    ]]
    build:
        sys-devel/gettext
        sys-devel/flex
        sys-libs/zlib
        virtual/pkg-config
        doc? (
            app-text/opensp
            dev-libs/libxml2:=[>=2.6.23]
            dev-libs/libxslt
        )
        llvm? ( dev-lang/clang:* )
    build+run:
        group/postgres
        user/postgres
        icu? ( dev-libs/icu:= )
        ldap? ( net-directory/openldap )
        llvm? ( dev-lang/llvm:=[<16] )
        pam? ( sys-libs/pam )
        perl? ( dev-lang/perl:= )
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl:= )
        )
        systemd? ( sys-apps/systemd )
        tcl? ( dev-lang/tcl:= )
        xml? (
            dev-libs/libxml2:=[>=2.6.23]
            dev-libs/libxslt
        )
        postgresql_extensions:sepgsql? ( security/libselinux )
    run:
        dev-db/postgresql-client
"

if ever at_least 13; then
    DEPENDENCIES+="
        build:
            doc? (
                app-text/docbook-sgml-dtd:4.5
            )
    "
else
    DEPENDENCIES+="
        build:
            doc? (
                app-text/docbook-sgml-dtd:4.2
            )
    "
fi

if ever at_least 14; then
    DEPENDENCIES+="
        build+run:
            lz4? ( app-arch/lz4 )
    "
fi

if ever at_least 15; then
    DEPENDENCIES+="
        build+run:
            zstd? ( app-arch/zstd[>=1.4.0] )
    "
fi

if ever at_least 16; then
    DEPENDENCIES+="
        build:
            sys-devel/bison[>=2.3]
            sys-devel/flex[>=2.5.35]
        build+run:
            perl? ( dev-lang/perl:=[>=5.14] )
    "
fi

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    icu
    # 'kerberos krb5'
    ldap
    llvm
    pam
    perl
    python
    'ssl openssl'
    systemd
    tcl
    'xml libxml'
    'xml libxslt'
)

if ever at_least 14; then
    DEFAULT_SRC_CONFIGURE_OPTION_WITHS+=(
        lz4
    )
fi

if ever at_least 15; then
    DEFAULT_SRC_CONFIGURE_OPTION_WITHS+=(
        zstd
    )
fi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CLANG=$(exhost --target)-clang
    --enable-nls
    --with-system-tzdata=/usr/share/zoneinfo
    --with-uuid=e2fs
    --with-zlib
    --bindir=/usr/$(exhost --target)/libexec/postgresql-${SLOT}
    --datadir=/usr/share/postgresql-${SLOT}
    --docdir=/usr/share/doc/postgresql-${SLOT}
    --mandir=/usr/share/postgresql-${SLOT}/man
    --includedir=/usr/$(exhost --target)/include/postgresql-${SLOT}
    --libdir=/usr/$(exhost --target)/lib/postgresql-${SLOT}
)

postgresql-build_pkg_pretend() {
    if [[ -z ${POSTGRESQL_MOVE_TO_SLOTS} ]] && has_version --root "${CATEGORY}/${PN}:0" ; then
        ewarn "To migrate to the slotted versions of PostgreSQL, you have to dump/reload your database."
        ewarn "When you've done this, please set 'POSTGRESQL_MOVE_TO_SLOTS=YesPlease', to continue the upgrade."
        die "Dump your databases before doing the upgrade of PostgreSQL."
    fi

    if [[ -f "${ROOT}"/etc/tmpfiles.d/${PN}.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/${PN}.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/${PN}.conf and can be safely removed after upgrade"
        ewarn "if you did not make any changes to it."
    fi

    # Pending genesis integration:
    # Upgrading to any version requires starting/stopping the PostgreSQL server.
}

postgresql-build_pkg_setup() {
    if option llvm; then
        providers_set "llvm $(get_best_slot_dependency dev-lang/llvm {15..13})"
        providers_set "clang $(get_best_slot_dependency dev-lang/clang {15..13})"
    fi
}

postgresql-build_src_prepare() {
    default

    if ! ever at_least 15; then
        # move CREATE TABLESPACE test (which is meant to fail) to ${WORK}
        edo sed -e "s:/no/such/location:${WORK}&:" -i src/test/regress/{input,output}/tablespace.source
    fi

    # use /run for unix sockets
    edo sed -e "/DEFAULT_PGSOCKET_DIR/s:/tmp:/run/postgresql:" -i src/include/pg_config_manual.h
}

postgresql-build_src_emake_contrib_one() {
    emake -C "contrib/$1" DESTDIR="${IMAGE}" $2
}

# The contrib directory is somewhat messy.
# It contains server-side administrative tools (usually very tightly coupled
# to the internal API and may or may not need a LOAD command in the db to be
# used), extensions (each versioned independently, loosely coupled to the
# internal API and needing a CREATE EXTENSION command in the db to be used),
# command line utilities (not coupled to the internal API, in fact, upstream
# recommends using the most recent available always instead of using the
# respective versions on each cluster) and also others.
postgresql-build_src_emake_contrib() {
    for pg_ext in $POSTGRESQL_EXTENSIONS; do
        postgresql-build_src_emake_contrib_one $pg_ext $1
    done

    # who wants perl, also wants bool_plperl
    if ever at_least 13 && option perl; then
        postgresql-build_src_emake_contrib_one bool_plperl $1
    fi

    # who wants perl, also wants jsonb_plperl
    if option perl; then
        postgresql-build_src_emake_contrib_one jsonb_plperl $1
    fi

    # who wants python, also wants jsonb_plpython
    if option python; then
        postgresql-build_src_emake_contrib_one jsonb_plpython $1
    fi

    # who wants hstore and perl, also wants hstore_plperl
    if option postgresql_extensions:hstore && option perl; then
        postgresql-build_src_emake_contrib_one hstore_plperl $1
    fi

    # who wants hstore and python, also wants hstore_plpython
    if option postgresql_extensions:hstore && option python; then
        postgresql-build_src_emake_contrib_one hstore_plpython $1
    fi

    # who wants lo, also wants vacuumlo
    if option postgresql_extensions:lo; then
        postgresql-build_src_emake_contrib_one vacuumlo $1
    fi

    # who wants ltree and python, also wants ltree_plpython
    if option postgresql_extensions:ltree && option python; then
        postgresql-build_src_emake_contrib_one ltree_plpython $1
    fi

    # most people want these built into the server but they only realize
    # it in the worst times, so let's get them ready upfront
    postgresql-build_src_emake_contrib_one adminpack $1
    postgresql-build_src_emake_contrib_one pg_stat_statements $1
    postgresql-build_src_emake_contrib_one pgrowlocks $1
}

postgresql-build_src_compile() {
    default

    postgresql-build_src_emake_contrib

    if option doc; then
        emake -C doc/src
    fi
}

# XXX: See http://wiki.postgresql.org/wiki/Detailed_installation_guides
# postgresql-build_pkg_config()

postgresql-build_src_install() {
    default

    # remove the client binaries and the translation files
    for binary in ecpg createdb dropdb createuser dropuser clusterdb ; do
        nonfatal edo rm "${IMAGE}"/usr/$(exhost --target)/libexec/postgresql-${SLOT}/${binary}
        nonfatal edo rm "${IMAGE}"/usr/share/locale/*/LC_MESSAGES/${binary}-${SLOT}.mo
        nonfatal edo find "${IMAGE}"/usr/ -type d -empty -delete
    done

    install_systemd_files

    # rename the unit file
    edo mv "${IMAGE}/${SYSTEMDSYSTEMUNITDIR}/"postgresql{,-${SLOT//./}}.service
    # update the unit file template
    edo sed -i -e "s/@SLOT@/${SLOT}/g" "${IMAGE}/${SYSTEMDSYSTEMUNITDIR}/postgresql-${SLOT//./}.service"

    diropts -g postgres -o postgres
    keepdir /var/lib/postgresql

    postgresql-build_src_emake_contrib install

    if option doc; then
        emake DESTDIR="${IMAGE}" -C doc/src install
    fi
}

# TODO: fix tests on earthdistance and test_decoding
postgresql-build_src_test() {
    esandbox allow_net "unix:${TEMP}/.s.PGSQL.*"
    esandbox allow_net --connect "unix:${TEMP}/.s.PGSQL.*"
    esandbox allow_net --connect "unix:/run/uuidd/request"
    esandbox allow_net --connect "inet:0.0.0.0/0@53"
    esandbox allow_net --connect "inet:127.0.0.1@9"

    PG_REGRESS_SOCK_DIR="${TEMP}" default

    esandbox allow_net "unix:${WORK}/contrib/pg_upgrade/.s.PGSQL.*"
    esandbox allow_net --connect "unix:${WORK}/contrib/pg_upgrade/.s.PGSQL.*"
    PG_REGRESS_SOCK_DIR="${TEMP}" postgresql-build_src_emake_contrib check
    esandbox disallow_net "unix:${WORK}/contrib/pg_upgrade/.s.PGSQL.*"
    esandbox disallow_net --connect "unix:${WORK}/contrib/pg_upgrade/.s.PGSQL.*"

    esandbox disallow_net "unix:${TEMP}/.s.PGSQL.*"
    esandbox disallow_net --connect "unix:${TEMP}/.s.PGSQL.*"
    esandbox disallow_net --connect "unix:/run/uuidd/request"
    esandbox disallow_net --connect "inet:0.0.0.0/0@53"
    esandbox disallow_net --connect "inet:127.0.0.1@9"
}

postgresql-build_pkg_postinst() {
    einfo "Before you can use the database, you must initialize it:"
    einfo "    su postgres -c '/usr/host/libexec/postgresql-${SLOT}/initdb /var/lib/postgresql/${SLOT}/data'"
    einfo ""
    einfo "To upgrade your database, follow the official upgrade guide:"
    einfo ""
    einfo "https://www.postgresql.org/docs/${SLOT}/static/upgrading.html"
}
