# Copyright 2008, 2009 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=gdraheim tag=v${PV} ]
require cmake

SUMMARY="A library to read zip files"
DESCRIPTION="
The implementation is based only on the (free) subset of compression with the zlib algorithm.
"

LICENCES="|| ( LGPL-2 MPL-1.1 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    sdl
"

DEPENDENCIES="
    build:
        app-text/xmlto
        dev-lang/python:*[>=3.5]
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        sdl? ( media-libs/SDL:0 )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DMSVC_STATIC_RUNTIME:BOOL=TRUE
    -DZZIP_COMPAT:BOOL=TRUE
    -DZZIP_HTMLSITE:BOOL=FALSE
    -DZZIP_HTMPAGES:BOOL=FALSE
    -DZZIP_INSTALL_ACLOCAL:BOOL=TRUE
    -DZZIP_INSTALL_BINS:BOOL=TRUE
    -DZZIP_LIBTOOL:BOOL=TRUE
    -DZZIP_MANPAGES:BOOL=TRUE
    -DZZIP_TESTCVE:BOOL=FALSE
    -DZZIP_PKGCONFIG:BOOL=TRUE
    -DZZIPBINS:BOOL=TRUE
    -DZZIPDOCS:BOOL=TRUE
    -DZZIPFSEEKO:BOOL=TRUE
    -DZZIPMMAPPED:BOOL=TRUE
    -DZZIPWRAP:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'sdl ZZIPSDL'
)

CMAKE_SRC_TEST_PARAMS=( -j1 )

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e 's|DESTINATION share/zziplib|DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake|' \
        -i zzip/CMakeLists.txt
}

