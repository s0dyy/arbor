# Copyright 2007 Bryan Østergaard
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="Open source SSL and TLS implementation and cryptographic library"
HOMEPAGE="https://www.openssl.org"
DOWNLOADS="${HOMEPAGE}/source/${PNV/_/-}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/news/changelog.html [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/ [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/news/announce.html [[ lang = en ]]"

if ever at_least 2; then
    SOVER="$(ever major)"
    SLOT="${SOVER}"
    LICENCES="Apache-2.0"
else
    SOVER="$(ever range 1-2)"
    SLOT="0" # clean upgrade path for 1.1
    LICENCES="${PN}"
fi

MYOPTIONS="parts: binaries configuration development documentation libraries"

WORK="${WORKBASE}/${PNV/_/-}"

DEPENDENCIES="
    build+run:
        !dev-libs/libressl:* [[
            description = [ LibreSSL is a drop-in replacement for OpenSSL with same library name ]
            resolution = uninstall-blocked-after
        ]]
"

# Tests don't run in parallel
DEFAULT_SRC_TEST_PARAMS=( -j1 )

_openssl_host_os() {
    # local configuration=(
    #   alpha64:linux-alpha-gcc # TODO(compnerd) Alpha64 ev56+ needs -mcpu, linux-alpha+bwx-gcc
    #   amd64:linux-x86_64
    #   arm:linux-armv4 # TODO(compnerd) do we want to port android armv7 to Linux?
    #   ia64:linux-ia64 # TODO(compnerd) ICC should be linux-ia64-icc
    #   ppc:linux-ppc
    #   ppc64:linux-ppc64
    #   s390:linux${MULTIBUILD_TARGET}-s390x
    #   sparc:linux-sparcv9 # XXX(compnerd) do we even care about sparc v8?
    #   sparc64:linux64-sparcv9
    #   x86:linux-elf # TODO(compnerd) ICC should be linux-ia32-icc
    # )
    case "$(exhost --target)" in
    arm*-*-linux-*)
        echo linux-armv4
    ;;
    aarch64-*-linux-*)
        echo linux-aarch64
    ;;
    i686-*-linux-*)
        echo linux-elf
    ;;
    x86_64-*-linux-*)
        echo linux-x86_64
    ;;
    *)
        die "unknown OpenSSL host/os for $(exhost --target)"
    ;;
    esac
}

_openssl_emake() {
    local mymake=(
        LIBDIR=lib
        MANDIR=/usr/share/man
        DOCDIR=/usr/share/doc/${PNVR}
    )

    emake "${mymake[@]}" "${@}"
}

openssl_src_prepare() {
    if ever at_least 3.0; then
        # 80-test_cmp_http_data: run the server on port 65535
        edo sed \
            -e 's:port = 0:port = 65535:g' \
            -i test/recipes/80-test_cmp_http_data/Mock/server.cnf
        # remove failing test, last checked: 3.2.0
        edo rm test/recipes/01-test_symbol_presence.t
    fi

    default
}

openssl_src_configure() {
    # LDFLAGS is out of the question thanks to the 'clever':
    # DO_GNU_APP=LDFLAGS="$(CFLAGS) -Wl,-rpath,$(LIBRPATH)"
    # We would like to use SHARED_LDFLAGS but that only works for .so's
    # So instead we just cheat and add LDFLAGS to CFLAGS so it gets
    # everywhere it needs to be along with some places where its harmless.
    export CFLAGS="${CFLAGS} ${LDFLAGS}"

    edo "${WORK}"/Configure $(_openssl_host_os) \
        --test-sanity

    edo "${WORK}"/Configure $(_openssl_host_os) \
        --prefix=/usr/$(exhost --target)/lib/${PN}-${SOVER} \
        --openssldir=/etc/ssl \
        shared threads \
        $(expecting_tests "" "no-external-tests no-fuzz-afl no-fuzz-libfuzzer no-tests no-unit-test")
}

openssl_src_compile() {
    _openssl_emake
}

openssl_src_test() {
    # prevent proxy settings messing with (80-test_cmp_http_data) tests
    edo unset http_proxy https_proxy ftp_proxy

    # test uses io-uring which is denylisted under SydB☮x.
    edo rm test/recipes/30-test_afalg.t

    esandbox allow_net --connect "inet:127.0.0.1@65535"
    esandbox allow_net --connect "inet6:::1@65535"
    case $(esandbox api) in
    3)
        # We're in a net ns so allowing any is OK.
        esandbox allow_net "ANY@0"
        esandbox allow_net "ANY@65535"
        ;;
    esac
    default
    esandbox disallow_net --connect "inet:127.0.0.1@65535"
    esandbox disallow_net --connect "inet6:::1@65535"
    case $(esandbox api) in
    3)
        # We're in a net ns so allowing any is OK.
        esandbox disallow_net "ANY@0"
        esandbox disallow_net "ANY@65535"
        ;;
    esac
}

openssl_src_install() {
    _openssl_emake -j1 DESTDIR="${IMAGE}" install

    local prefix=/usr/$(exhost --target)
    local verprefix=${prefix}/lib/${PN}-${SOVER}

    local lib
    local alternatives=()

    for lib in crypto ssl; do
        dosym {${PN}-${SOVER}/,/usr/$(exhost --target)/}lib/lib${lib}.so.${SOVER}
        alternatives+=(
            ${prefix}/lib/lib${lib}.so          ${verprefix}/lib/lib${lib}.so
        )
    done
    alternatives+=(
        ${prefix}/lib/libcrypto.a               ${verprefix}/lib/libcrypto.a
        ${prefix}/lib/libssl.a                  ${verprefix}/lib/libssl.a
        ${prefix}/lib/pkgconfig/libcrypto.pc    ${verprefix}/lib/pkgconfig/libcrypto.pc
        ${prefix}/lib/pkgconfig/libssl.pc       ${verprefix}/lib/pkgconfig/libssl.pc
        ${prefix}/lib/pkgconfig/openssl.pc      ${verprefix}/lib/pkgconfig/openssl.pc
        ${prefix}/include/openssl               ${verprefix}/include/openssl
    )

    # alternatives to support multiple slots, architecture dependent files
    alternatives_for \
        _$(exhost --target)_${PN} ${SOVER} ${SOVER} \
        "${alternatives[@]}"

    # alternatives to support multiple slots, architecture dependent executables
    alternatives=(
        ${prefix}/bin/c_rehash  ${verprefix}/bin/c_rehash
        ${prefix}/bin/openssl   ${verprefix}/bin/openssl
    )
    alternatives_for \
        _$(exhost --target)_${PN}_bin ${SOVER} ${SOVER} \
        "${alternatives[@]}"

    if ! ever at_least 2; then
        # conflicts with shadow, avoid by overwriting the symlink with the actual
        # manpage
        edo mv "${IMAGE}"/usr/share/man/man1/{,openssl-}passwd.1
    fi

    # alternatives to support multiple slots, architecture independent files
    alternatives=()

    for file in "${IMAGE}"/usr/share/man/*/*; do
        alternatives+=(
            ${file#${IMAGE}}
            ${file#${IMAGE}}-${SOVER}
        )
    done

    alternatives_for \
        _${PN} ${SOVER} ${SOVER} \
        "${alternatives[@]}"

    keepdir /etc/ssl/{certs,private}
    emagicdocs

    expart libraries /usr/$(exhost --target)/lib
    expart binaries /usr/$(exhost --target)/{,lib/${PN}-${SOVER}/}bin
    expart development /usr/$(exhost --target)/{,lib/${PN}-${SOVER}/}{include,lib/pkgconfig}
    expart configuration /etc
    expart --exclude configuration /etc/env.d
    expart documentation /usr/share/{doc,man}
}

