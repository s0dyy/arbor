# Copyright 2019 Danilo Spinella <danyspin97@protonamail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part on "s6-exherbo-scm.exheres-0", which is
#   Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Based in part on "beginning-scm.exheres-0", which is
#   Copyright 2015-2018 Kylie McClain <somasis@exherbo.org>

require gitlab [ prefix=https://gitlab.exherbo.org user=exherbo-misc ]

export_exlib_phases src_prepare src_compile pkg_postinst

SUMMARY="A 66 policy for Exherbo"

LICENCES="BSD-0"
SLOT="0"

MYOPTIONS="
    libc: musl
"

DEPENDENCIES="
    run:
        sys-apps/66
        sys-apps/s6-linux-utils
        sys-apps/s6-portable-utils
        libc:musl? (
            sys-apps/socklog
        )
"

DEFAULT_SRC_INSTALL_PARAMS=(
    DESTDIR="${IMAGE}"
    PREFIX=/usr/
)

66-exherbo_src_prepare() {
    if option libc:musl; then
        # Use socklog to read system messages s6-ipcserver
        # https://github.com/just-containers/s6-overlay/issues/214#issuecomment-337686761
        edo sed -i 's/syslog/socklog/' "${WORK}"/usr/share/66/service/boot/Boot-log
    fi

    default
}

66-exherbo_src_compile() {
    :
}

66-exherbo_pkg_postinst() {
    # First setup
    if [[ ! -f /var/lib/66/66-exherbo_installed ]]; then
        # Add boot tree but don't enable it
        edo 66-tree -n boot
        edo 66-enable -t boot Boot

        # Add a system tree, make it current and enable it
        edo 66-tree -ncE system

        edo touch /var/lib/66/66-exherbo_installed
    fi
}

