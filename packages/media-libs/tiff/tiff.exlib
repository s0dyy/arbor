# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_install

SUMMARY="Library and utilities for handling TIFF-files"
HOMEPAGE="https://libtiff.gitlab.io/libtiff/"
DOWNLOADS="https://download.osgeo.org/libtiff/${PNV}.tar.xz"

LICENCES="MIT"
MYOPTIONS="
    libdeflate [[
        description = [ Use libdeflate for faster Zip/Deflate (de-)compression ]
    ]]
    webp [[ description = [ Support the webp compression format ] ]]
    zstd
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        app-arch/xz
        sys-libs/zlib
        libdeflate? ( app-arch/libdeflate )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        webp? ( media-libs/libwebp:= )
        zstd? ( app-arch/zstd )
    run:
        !media-libs/tiff:0[<4.4.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cxx
    --enable-ld-version-script
    --enable-lzma
    --enable-zlib
    --disable-jbig
    --disable-lerc
    --disable-static
    --with-docdir=/usr/share/doc/${PNVR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    libdeflate
    webp
    zstd
)

if ever at_least 4.6.0; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-docs
        --enable-tools
        --disable-contrib
        --disable-deprecated
        --disable-sphinx
        --disable-tools-unsupported
    )
    DEFAULT_SRC_CONFIGURE_TESTS=(
        '--enable-tests --disable-tests'
    )
else
    MYOPTIONS+="
        opengl
    "
    DEPENDENCIES+="
        build+run:
            opengl? (
                x11-dri/freeglut
                x11-dri/mesa
                x11-libs/libX11
                x11-libs/libICE
            )
    "
    DEFAULT_SRC_CONFIGURE_OPTION_WITHS+=(
        'opengl x'
    )
fi

tiff_src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives=(
        /usr/${host}/bin/tiffcp                              tiffcp-${SLOT}
        /usr/${host}/bin/tiffdump                            tiffdump-${SLOT}
        /usr/${host}/bin/tiffinfo                            tiffinfo-${SLOT}
        /usr/${host}/bin/tiffset                             tiffset-${SLOT}
        /usr/${host}/bin/tiffsplit                           tiffsplit-${SLOT}
        /usr/${host}/include/tiff.h                          tiff-${SLOT}.h
        /usr/${host}/include/tiffconf.h                      tiffconf-${SLOT}.h
        /usr/${host}/include/tiffio.h                        tiffio-${SLOT}.h
        /usr/${host}/include/tiffio.hxx                      tiffio-${SLOT}.hxx
        /usr/${host}/include/tiffvers.h                      tiffvers-${SLOT}.h
        /usr/${host}/lib/lib${PN}.so                         lib${PN}-${SLOT}.so
        /usr/${host}/lib/lib${PN}.la                         lib${PN}-${SLOT}.la
        /usr/${host}/lib/lib${PN}xx.so                       lib${PN}xx-${SLOT}.so
        /usr/${host}/lib/lib${PN}xx.la                       lib${PN}xx-${SLOT}.la
        /usr/${host}/lib/pkgconfig/lib${PN}-$(ever major).pc lib${PN}-$(ever major)-${SLOT}.pc
    )

    if ! ever at_least 4.6.0; then
        arch_dependent_alternatives+=(
            /usr/${host}/bin/fax2ps                              fax2ps-${SLOT}
            /usr/${host}/bin/fax2tiff                            fax2tiff-${SLOT}
            /usr/${host}/bin/pal2rgb                             pal2rgb-${SLOT}
            /usr/${host}/bin/ppm2tiff                            ppm2tiff-${SLOT}
            /usr/${host}/bin/raw2tiff                            raw2tiff-${SLOT}
            /usr/${host}/bin/tiff2bw                             tiff2bw-${SLOT}
            /usr/${host}/bin/tiff2rgba                           tiff2rgba-${SLOT}
            /usr/${host}/bin/tiffcmp                             tiffcmp-${SLOT}
            /usr/${host}/bin/tiffcrop                            tiffcrop-${SLOT}
            /usr/${host}/bin/tiffdither                          tiffdither-${SLOT}
            /usr/${host}/bin/tiffgt                              tiffgt-${SLOT}
            /usr/${host}/bin/tiffmedian                          tiffmedian-${SLOT}
        )
        # tools-unsupported
        arch_dependent_alternatives+=(
            /usr/${host}/bin/tiff2pdf                            tiff2pdf-${SLOT}
            /usr/${host}/bin/tiff2ps                             tiff2ps-${SLOT}
        )
    fi

    edo pushd "${IMAGE}"
    for manpage in usr/share/man/man1/*.1 ; do
        manfile=${manpage##*/}
        other_alternatives+=( "/${manpage}" "${manfile/%.1/-${SLOT}.1}" )
    done
    for manpage in usr/share/man/man3/*.3tiff ; do
        manfile=${manpage##*/}
        other_alternatives+=( "/${manpage}" "${manfile/%.3tiff/-${SLOT}.3tiff}" )
    done
    edo popd

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

