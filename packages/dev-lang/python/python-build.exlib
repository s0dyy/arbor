# Copyright 2007-2007 Bryan Østergaard
# Copyright 2008 Ali Polatel
# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'python-2.5.2-r4.ebuild' from Gentoo, which is:
#  Copyright 1999-2008 Gentoo Foundation

MY_PV=${PV/_beta/b}
MY_PV=${MY_PV/_rc/rc}
MY_PNV="Python-${MY_PV}"
# Alpha/Beta tarballs are in the same directory as the first actual release tarball
MY_DIR_PV=${PV/_beta*}
MY_DIR_PV=${MY_DIR_PV/_rc*}

# python: no has_lib or multibuild as it needs only the pyc part of the exlib
require flag-o-matic python [ has_lib=false multibuild=false blacklist=none ] alternatives
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

export_exlib_phases pkg_pretend src_prepare src_configure src_test src_install

SUMMARY="Python interpreter"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/ftp/${PN}/${MY_DIR_PV}/${MY_PNV}.tar.xz"

UPSTREAM_DOCUMENTATION="https://docs.${PN}.org/$(ever range 1-2)/"

LICENCES="PSF-2.2"
SLOT="$(ever range 1-2)"
MYOPTIONS="examples gdbm readline sqlite tk

    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        dev-libs/expat
        dev-libs/libffi:=
        sys-libs/ncurses
        sys-libs/zlib[>=1.1.3]
        gdbm? ( sys-libs/gdbm )
        !libc:musl? ( dev-libs/libxcrypt:= )
        readline? ( sys-libs/readline:=[>=4.1] )
        sqlite? ( dev-db/sqlite:3[>=3.0.8] )
        tk? ( dev-lang/tk[>=8.0] )
    post:
        app-admin/eclectic-python:$(ever major)
"

if ever at_least 3.11.0 ; then
    DEPENDENCIES+="
        build+run:
            app-crypt/libb2
    "
fi

# >= 3.7.0 needs X509_VERIFY_PARAM_set1_host() provided by libressl >=2.7/openssl >=1.0.2
# >= 3.10.0 needs SSL_CTX_get_security_level() provided by libressl >=3.6/openssl >=1.1.0
if ever at_least 3.10.0; then
    DEPENDENCIES+="
        build+run:
            providers:libressl? ( dev-libs/libressl:=[>=3.6] )
            providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
    "
else
    DEPENDENCIES+="
        build+run:
            providers:libressl? ( dev-libs/libressl:=[>=2.7] )
            providers:openssl? ( dev-libs/openssl:=[>=1.0.2] )
    "
fi

if ever at_least 3.8.9; then
    DEPENDENCIES+="
        build:
            sys-devel/autoconf-archive
        build+run:
            sys-apps/util-linux[>=2.20] [[ note = [ for libuuid ] ]]
    "
fi

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_COMPILE_PARAMS=(
    # Unset them explicitly or they'll end up in sysconfig, cf. X_NODIST in
    # src_configure
    CPPFLAGS= CFLAGS= LDFLAGS=
    # We consider them as arch-dependent
    SCRIPTDIR='$(LIBDIR)'
)

DEFAULT_SRC_INSTALL_PARAMS=(
    # We consider them as arch-dependent
    SCRIPTDIR='$(LIBDIR)'
)

PYTHON_BYTECOMPILE_EXCLUDES=( bad_coding badsyntax lib2to3/tests/data site-packages )

python-build_pkg_pretend() {
    # for native builds, the version we are about to build does the .pyc compiling
    if ! exhost --is-native -q && [[ ! -x /usr/$(exhost --build)/bin/python${SLOT} ]] ; then
        ewarn "You need to install python:${SLOT} for $(exhost --build) if "
        ewarn "you want to build python:${SLOT} for $(exhost --target)"
        die "install python:${SLOT} !"
    fi
}

python-build_src_prepare() {
    expatch "${FILES}/${SLOT}/"

    edo cp "${FILES}"/"${PN}"-config.py .

    edo sed -i -e "s:@@SLOT@@:${SLOT}:g" \
               -e "s:@@EXHERBO_TARGET@@:$(exhost --target):g" \
               -e "s:@@EXHERBO_TOOL_PREFIX@@:$(exhost --tool-prefix):g" \
        "${PN}"-config.py

    # Needs to run `host` code to regen this
    edo touch Include/graminit.h Python/graminit.c

    eautoreconf
}

python-build_src_configure() {
    if ever at_least 3.12 ; then
        edo cat > Modules/Setup.local <<EOF
*disabled*
nis
$(option gdbm || echo '_gdbm _dbm')
$(option readline || echo 'readline')
$(option sqlite || echo '_sqlite3')
$(option tk || echo '_tkinter')
EOF
    else
        local disable="ndbm" # not available

        # Avoid sneaky linking to libtirpc
        disable+=" nis"

        option gdbm || disable+=" _gdbm _dbm"
        option readline || disable+=" readline"
        option sqlite   || disable+=" _sqlite3"
        option tk       || disable+=" _tkinter"
        export PYTHON_DISABLE_MODULES="${disable}"
    fi

    # -fwrapv: http://bugs.python.org/issue11149
    export OPT="-fwrapv"

    # Use X_NODIST to pass system {C,LD}FLAGS, otherwise they'll end up in
    # sysconfig and may leak into external modules
    local -x CFLAGS_NODIST=${CFLAGS}
    local -x LDFLAGS_NODIST=${LDFLAGS}
    local -x CFLAGS= LDFLAGS=

    # Avoid our LIBC option interfering with python's build system
    unset LIBC

    local params=(
        --enable-ipv6
        --enable-shared
        --with-system-expat
        --without-ensurepip
        ac_cv_file__dev_ptc=no
        ac_cv_file__dev_ptmx=yes
        $(option_enable sqlite loadable-sqlite-extensions)
    )

    if ever at_least 3.9; then
        params+=( --without-static-libpython )
    fi

    if ever at_least 3.11 ; then
        params+=( --with-pkg-config=yes )

        if ! exhost --is-native -q ; then
            params+=( --with-build-python=/usr/$(exhost --build)/bin/python${SLOT} )
        fi
    fi

    if ever at_least 3.12 ; then
        # Experimental, apparently part of llvm, but we don't seem to include it
        params+=( --disable-bolt )
    else
        params+=( --with-system-ffi )
    fi

    econf "${params[@]}"
}

python-build_src_test() {
    unset DISPLAY

    # for argparse test_help_with_metavar
    local COLUMNS=80

    python_enable_pyc

    # needed for 32bit tests
    esandbox allow_net --connect "inet:127.0.0.1@1024-65535"
    esandbox allow_net --connect "inet6:::1@1024-65535"

    # Many tests create sockets in the temporary directory; including but not limited to
    # test_multiprocessing, other tests using multiprocessing, test_logging, test_socketserver.
    # Rather than list them all individually, which gets out of date quickly due to changes between
    # python versions, cover them with a wildcard match.
    esandbox allow_net "unix:${TEMP%/}/**"
    ever at_least 3.12 && esandbox allow_net "unix:${WORK}/build/**"

    # for test_grp
    esandbox allow_net "unix-abstract:/**/userdb-*"

    # for test_uuid (avoid log spam, it doesn't actually require access)
    esandbox addfilter_net --connect "unix:/run/uuidd/request"

    # for test_spwd, we hide /etc/shadow but test expects EPERM.
    # Use allow rather than allow_stat for syd-1 compat.
    # TODO: ^^ allow_stat is available for syd-1 in paludis-scm.
    esandbox allow /etc/shadow # SAFETY: read is still denied!

    # rerun failed tests in verbose mode (regrtest -w)
    # disable tests (regrtest -x)
    local -x EXTRATESTOPTS="-j ${EXJOBS:-1} -w -x ${DISABLE_TESTS[@]}"

    emake test

    esandbox rmfilter_net --connect "unix:/run/uuidd/request"
    esandbox disallow_net "unix-abstract:/**/userdb-*"
    ever at_least 3.12 && esandbox disallow_net "unix:${WORK}/build/**"
    esandbox disallow_net "unix:${TEMP%/}/***"
    esandbox disallow_net --connect "inet:127.0.0.1@1024-65535"
    esandbox disallow_net --connect "inet6:::1@1024-65535"

    python_disable_pyc
}

python-build_src_install() {
    default

    target=$(exhost --target)
    # alternatives handling
    eval ALTERNATIVES_python$(ever major)_DESCRIPTION="Default\ python$(ever major)\ version"
    alternatives=("${PN}$(ever major)" "${SLOT}" "${SLOT}")

    # for backward compatibility, have plat-linux3 and plat-linux2 folders
    # as long as they are identical, this shouldn't be a problem
    pushd "${IMAGE}"/usr/${target}/lib/python${SLOT}/
    if [[ -d plat-linux2 ]]; then
        edo cp -r plat-linux2 plat-linux3
    elif [[ -d plat-linux3 ]]; then
        edo cp -r plat-linux3 plat-linux2
    fi
    popd

    edo rm "${IMAGE}"/usr/${target}/bin/python$(ever major)
    edo rm "${IMAGE}"/usr/${target}/bin/python$(ever major)-config
    edo rm "${IMAGE}"/usr/${target}/lib/pkgconfig/python$(ever major).pc
    edo rm "${IMAGE}"/usr/share/man/man1/python$(ever major).1

    edo rm "${IMAGE}"/usr/${target}/bin/{2to3,idle3,pydoc3}

    edo mv "${IMAGE}"/usr/${target}/lib/libpython$(ever major){,-${SLOT}}.so

    alternatives+=(
        /usr/${target}/lib/libpython$(ever major).so libpython$(ever major)-${SLOT}.so
    )

    # "m" suffix (meaning pymalloc) was removed in 3.8. Still keep it for previous versions.
    edo rm "${IMAGE}"/usr/${target}/lib/pkgconfig/python$(ever major)-embed.pc

    if ever at_least 3.9; then
        :
    else
        edo mv "${IMAGE}"/usr/${target}/lib/{python${SLOT}/config-${SLOT}*,}/libpython${SLOT}.a
    fi

    alternatives+=(
        /usr/${target}/lib/pkgconfig/python$(ever major).pc python-${SLOT}.pc
        /usr/${target}/lib/pkgconfig/python$(ever major)-embed.pc python-${SLOT}-embed.pc
        /usr/${target}/bin/2to3-$(ever major) 2to3-${SLOT}
        /usr/${target}/bin/idle$(ever major) idle${SLOT}
        /usr/${target}/bin/pydoc$(ever major) pydoc${SLOT}
        /usr/${target}/bin/python$(ever major) python${SLOT}
        /usr/${target}/bin/python$(ever major)-config python${SLOT}-config
        /usr/share/man/man1/python$(ever major).1 python${SLOT}.1
    )

    alternatives_for "${alternatives[@]}"

    # The Makefile also does this but tries to use `build` python with `target` python modules
    if ! exhost --is-native -q ; then
        PYTHON_ABIS=${SLOT} python_bytecompile
        edo python${SLOT} -m lib2to3.pgen2.driver "${IMAGE}"/usr/${target}/lib/python${SLOT}/lib2to3/Grammar.txt
        edo python${SLOT} -m lib2to3.pgen2.driver "${IMAGE}"/usr/${target}/lib/python${SLOT}/lib2to3/PatternGrammar.txt
    fi
}

