# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'swig-1.3.35.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require sourceforge [ suffix=tar.gz ]
require ruby [ multibuild=false with_opt=true ]
require python [ multibuild=false blacklist=none with_opt=true ]

SUMMARY="Simplified Wrapper and Interface Generator"
HOMEPAGE="http://www.${PN}.org"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/Doc$(ever range 1-2)/SWIGDocumentation.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/Release/RELEASENOTES [[ lang = en ]]"

LICENCES="GPL-3 as-is"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    lua
    perl
"

RESTRICT="test"

# TODO: python3 has an extra switch
DEPENDENCIES="
    build+run:
        dev-libs/pcre
        lua? ( dev-lang/lua:= )
        perl? ( dev-lang/perl:=[=5*] )
"
#    test-expensive:
#        dev-libs/boost[>=1.20.0]
#"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( NEW FUTURE )

src_configure() {
    local myconf=()

    myconf+=(
        --with-pcre
        --without-{csharp,d,go,guile,java,javascript,mono,mzscheme}
        --without-{ocaml,octave,php,r,scilab,tcl}
        --without-doc
        # Breaks tests
        --disable-ccache
        $(option_enable lua)
        $(option_enable perl perl5)
        $(option_enable ruby ruby ruby$(ruby_get_abi))
    )

    if option python ; then
        if [[ $(python_get_abi) == 3.* ]] ; then
            myconf+=(
                PYTHON3="/usr/$(exhost --target)/bin/python$(python_get_abi)"
                --with-python3
                --without-python
            )
        else
            myconf+=(
                --with-python
                --without-python3
            )
        fi
    else
        myconf+=(
            --without-python
            --without-python3
        )
    fi

    econf "${myconf[@]}"
}

# TODO/FIXME: teach tests to use prefixed tools
#src_test_expensive() {
#    emake check
#}

src_install() {
    default

    if option doc; then
        dodir /usr/share/doc/${PNVR}
        insinto /usr/share/doc/${PNVR}
        doins -r Doc/{Devel,Manual}
    fi
}

