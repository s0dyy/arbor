# Copyright 2012 William Orr <will@worrbase.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=dell tag="v${PV}" ] bash-completion \
    systemd-service [ systemd_files=[ dkms.service ] ]

SUMMARY="Dynamically rebuild kernel modules"
DESCRIPTION="
DKMS includes utilities to dynamically rebuild kernel modules when the kernel is updated. It also
allows developers to build DKMS-enabled tarballs, rpms and debs that will be rebuilt on kernel
update.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        app-shells/bash
    suggestion:
        app-arch/dpkg [[
            description = [ Build DKMS-enabled debs ]
        ]]
        app-arch/rpm [[
            description = [ Build DKMS-enabled rpms ]
        ]]
        sys-boot/mokutil [[
            description = [ Module signing for Secure Boot enabled UEFI systems ]
        ]]
"

BASH_COMPLETIONS=( dkms.bash-completion )

DEFAULT_SRC_INSTALL_PARAMS=(
    SBIN="/usr/$(exhost --target)/bin"
    LIBDIR="/usr/$(exhost --target)/lib/dkms"
)

src_prepare() {
    edo sed \
        -e '/bash-completion/ d' \
        -e '/mkdir/ s|\$(BASHDIR)||' \
        -i Makefile

    default
}

src_compile() {
    :
}

src_install() {
    default

    keepdir /etc/dkms/framework.conf.d
    keepdir /var/lib/${PN}

    bash-completion_src_install
    install_systemd_files
}

